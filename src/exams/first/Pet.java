package exams.first;

public class Pet {
	
	private String name;
	private int age;
	
	public Pet() {
		name = "tom";
		age = 1;
	}
	
	public Pet(String name, int age) {
		this.name = name;
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age > 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet p) {
		if (!name.equals(p.getName())) {
			return false;
		}else if (age != p.getAge()) {
            return false;
        }
        return true;
	}
	
	 public String toString() {
	        return name + " is " + age + " years old.";
	    }
	 
}
