package exams.first;

import java.util.Scanner;

public class Q3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Size of triangle: ");//prompt user for input
		int size = Integer.parseInt(input.nextLine());
		
		input.close();
		
		for (int row=0; row<size; row++) {
			
			for (int col= 0; col<=row; col++) {
				System.out.print("* ");	
				
			}
			System.out.println();
			
			
		}
		
		
		input.close();
		

	}

}
