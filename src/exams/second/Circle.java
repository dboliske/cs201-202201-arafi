package exams.second;

public class Circle extends Polygon {

	private double radius;
	
	public Circle() {
		super();
		radius = 1.0;
	}
	
	public double getRadius() {
		return radius;
	}


	public void setRadius(double radius) {
		if (radius > 0) {
			this.radius = radius;
		}
	}
	
	@Override
	public String toString() {
		return super.toString() + "," + " radius: " + radius;
	}


	@Override
	public double area() {
		double aValue = Math.PI * radius * radius;
		return aValue;
	}

	@Override
	public double perimeter() {
		double pValue = 2.0 * Math.PI * radius;
		return pValue;
	}

}
