package exams.second;

public class Classroom {

	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "hermann";
		roomNumber = "wh1";
		seats = 20;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String number) {
		this.roomNumber = number;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		if (seats >= 0) {
			this.seats = seats;
		}
		
	}
	
	@Override
	public String toString() {
		return "Building: " + building+ "," + " Room number: " + roomNumber + "," + " Seats: " + seats;
	}
	
	
	
}
