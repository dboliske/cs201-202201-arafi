package exams.second;

public class ComputerLab extends Classroom {

	private boolean computers;
	
	public ComputerLab() {
		super();
		computers = false;
	}

	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	public boolean hasComputers() {
		return computers;
	}
	
	@Override
	public String toString() {
		return super.toString() + " has " + computers;
	}
}
