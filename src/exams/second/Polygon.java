package exams.second;

public abstract class Polygon {

	protected String name;
	
	public Polygon() {
		name = "rectangle";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Name of polygon: " + name;
	}
	
	public abstract double area();
	public abstract double perimeter();
}
