package exams.second;

import java.util.Scanner;

public class QuestionFive {

	public static int search(double[] array, double value) {
		int step = (int)Math.sqrt(array.length);
		
		int end = Math.min(step, array.length - 1);
		
		return jumpSearch(array, value, step, end);
	}
				
	public static int jumpSearch(double[] array, double value, int step, int end) {
		
		if (array[end] < value && end < array.length) {
			end = end + step;
			return jumpSearch(array,value,step,end);
			
		} else {
			
			int step2 = end - step;
			
			for (int i = step2 ; i< array.length && i<=end; i++) {
				if (array[i] == value) {
					return i;
				}
			}
		}
				
		
		return -1;
	}


	
	public static void main(String[] args) {
		
		double[] num = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Search value: ");
		double value = Double.parseDouble(input.nextLine());
		
		int index = search(num, value);
		
		if (index == -1) {
			System.out.println(index);
		} else {
			System.out.println(value + " found at index " + index + ".");
		}

		input.close();
	}	
}
