package exams.second;

public class QuestionFour {

	public static String[] sort(String[] nArray) {
		for(int i = 0; i<nArray.length; i++) {
			int min = i;
			for (int j = i+1; j<nArray.length; j++) {
				if (nArray[j].compareToIgnoreCase(nArray[min])<0) {
					min = j;
				}
			}
			
			if (min != i) {
				String temp = nArray[i];
				nArray[i]= nArray[min];
				nArray[min]= temp;
			}
		}
		return nArray;
	}

	public static void main(String[] args) {
		String[] array = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		array = sort(array);
		
		for (String s : array) {
			System.out.print(s + " ");
		}

	}

}
