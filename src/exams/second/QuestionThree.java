package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionThree {

	public static ArrayList<Integer> sort(ArrayList<Integer> integer) {
		Integer temp;
		boolean done = false;
		
		do {
			done = true;
			for (int i=0; i<integer.size() - 1; i++) {
				if (integer.get(i+1).compareTo(integer.get(i)) < 0) {
					temp = integer.get(i+1);
					integer.set(i+1,integer.get(i));
					integer.set(i, temp);
					
					done = false;
				}
			}
		} while (!done);
		
		return integer;
	}
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Integer> n = new ArrayList<Integer>();
		
		boolean done = false;
		
		System.out.print("Enter number or type 'done': ");
		String value = input.nextLine();
		
		while(!done) {
			if (!value.equalsIgnoreCase("done")) {
				int num = Integer.parseInt(value);
				n.add(num);
				System.out.print("Enter number or type 'done': ");
				String m = input.nextLine();
				value = m;
			}else {
				done = true;
			}
			
		}
					
		sort(n)	;
			
		System.out.println("Minimum: " + n.get(0));
		System.out.println("Maximum: " + n.get(n.size()-1));
		
		input.close();

	}

}
