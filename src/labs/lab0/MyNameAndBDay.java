package labs.lab0;//Name: Abrar Shahariar Rafi; Course: CS201; Sec.# 01, Date: 1/14/22; "Output Name & Birthdate" 

public class MyNameAndBDay {

	public static void main(String[] args) {
		
		String name = "Abrar Shahariar Rafi"; //Declaring a string and assigning string value (Name)
		String month = "Oct.";//Declaring a string and assigning string value (Month)
		int date = 25; //Declaring an integer and assigning value (date)
		int year = 2001; //Declaring an integer and assigning value (year)
		
		System.out.println("My name is " + name + " and my birthdate is " + month + date + "," + year); //Print out the name and birthdate in a sentence
	}

}
