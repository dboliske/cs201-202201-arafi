package labs.lab0;//Name: Abrar Shahariar Rafi; Course: CS201; Sec.# 01, Date: 1/14/22; "Output a Square"

//Step1: Print 10 dots with a gap of 1 character between each dot to form a row
//Step2: Print 2 dots with a gap of 17 characters between them
//Step3: Repeat Step2 8 times
//Step4: Repeat step 1

public class Square {

	public static void main(String[] args) {
		System.out.println(". . . . . . . . . .");
		System.out.println(".                 .");
		System.out.println(".                 .");
		System.out.println(".                 .");
		System.out.println(".                 .");
		System.out.println(".                 .");
		System.out.println(".                 .");
		System.out.println(".                 .");
		System.out.println(".                 .");
		System.out.println(". . . . . . . . . .");
	}

}
