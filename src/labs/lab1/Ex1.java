package labs.lab1;

import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//create scanner for user input
		System.out.print("Enter name: ");//prompt the user to type name
		String name = input.nextLine();//read in the next line of text typed by the user
		System.out.println("Echo: " + name);//print what the user entered
		
		input.close();
	}

}
