package labs.lab1;

public class Ex2 {

	public static void main(String[] args) {
		int Father = 50; //creating variable and storing father's age
		int MyAge = 20; //creating variable and storing my age
		int BirthYear = 2001;//creating variable and storing my birth year
		int height = 59; //creating variable and storing my height in inches
		
		System.out.println("Difference between my father's age and mine: " + (Father - MyAge));//print out the difference between my age and my father's
		System.out.println("My birth year multiplied by 2: " + BirthYear*2);//print out the result of my birth year multiplied by 2
		System.out.println("My height in cm: " + height*2.54);// print out my height in cm (inches to cm conversion)
		System.out.println("My height in feet & inches: " + (height/12) + " feet " + height%12 + " inches ");// printout my height in feet and inches
	}

}
