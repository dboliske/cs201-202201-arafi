package labs.lab1;

import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//create scanner for user input
		
		System.out.print("Enter first name: ");//prompt for first name 
		String value = input.nextLine();// stores user input to value
		
		System.out.println(value.charAt(0));//prints out the first initial 
		input.close();
	}

}
