package labs.lab1;

import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//create scanner for user input
		
		System.out.print("Enter Temperature in Fahrenheit: ");// prompt user for temp in Fahrenheit
		double temp1 = Double.parseDouble(input.nextLine());
		
		System.out.println("Temperature conversion from Fahrenheit to Celsius: " + (temp1-32)/1.8);// printout the conversion fron fahrenheit to celsius
		
		System.out.print("Enter Temperature in Celsius: ");// prompt user for temp in Celsius
		double temp2 = Double.parseDouble(input.nextLine());
		
		System.out.println("Temperature conversion from Celsius to Fahrenheit: " + ((temp2*1.8)+32));// printout the conversion fron fahrenheit celsius to fahrenheit 
		input.close();
	}

}

//input(Fahrenheit): -20, 30, 80
//expected output in celsius: -28.8889, -1.11111, 26.6667
//actual output in celsius: -28.88888888888889, -1.1111111111111112, 26.666666666666664

//input(Celsius):-10, 30, 70
//expected output in fahrenheit: 14, 86, 158
//actual output in fahrenheit: 14.0, 86.0, 158.0

//the program works as expected