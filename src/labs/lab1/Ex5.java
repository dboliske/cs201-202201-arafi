package labs.lab1;

import java.util.Scanner;

public class Ex5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter a length in inches: ");//create scanner for user input
		double length = Double.parseDouble(input.nextLine());//prompt user for a length
		double l = length/12;//conversion to feet
		
		System.out.print("Enter a width in inches: ");
		double width = Double.parseDouble(input.nextLine());//prompt user for a width
		double w = width/12;//conversion to feet
		
		System.out.print("Enter a depth in inches: ");//prompt user for a depth
		double depth = Double.parseDouble(input.nextLine());
		double d = depth/12;//conversion to feet
		
		System.out.println("Amount of wood required to make the box in square feet: " + (2*(l*w)+2*(l*d)+2*(w*d)));// print out the area of the box in square feet
		input.close();
	}

}

//input(length):30, 8, 60
//input(width): 20, 6, 40
//input(depth): 10, 5, 30
//Area (expected): 15.27777778, 1.638888889, 75
//Area (actual):15.277777777777779, 1.6388888888888888, 75.0

//the program works as expected