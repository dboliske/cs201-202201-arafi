package labs.lab1;

import java.util.Scanner;

public class Ex6 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//create scanner for user input
		
		System.out.print("Enter length in inches: ");//prompt user for inches
		double inches = Double.parseDouble(input.nextLine());
		
		System.out.println("Length in cm: " + (inches*2.54));//printout conversion from inches to centimeter
		
		input.close();
	}

}
//input(length):10, 35, 86
//output(expected): 25.4, 88.9, 218.44
//output(actual): 25.4, 88.9, 218.44

//the program works as expected