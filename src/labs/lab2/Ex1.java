package labs.lab2;

import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//takes user input
		
		System.out.print("Size of square: ");//prompt user for input
		int size = Integer.parseInt(input.nextLine());
		
		input.close();
		
		for (int row=0; row<size; row++) {//looping for row until the value of size
			for (int col=0; col<size; col++) {//looping for column until the value of size
				System.out.print("* ");
			}
			System.out.println();
		}
	}

}
