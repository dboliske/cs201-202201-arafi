package labs.lab2;

import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//takes user input
		
		System.out.print("Enter a grade: ");//prompt user for input
		double grade = Double.parseDouble(input.nextLine());
		
		int count = 0;//initializing
		
		double sum =0.0;//initializing
		
		while (grade > -1) {//condition : enters loop if grade greater than -1
			
			sum += grade;//addition of grade
			count ++;//update
			System.out.print("Enter a grade or enter -1 to exit: ");//prompt user for more input
			grade = Double.parseDouble(input.nextLine());
			
		}
	
		System.out.println("The average is: " + (sum/count));//printout the average grade
		
		input.close();
	}

}
