package labs.lab2;

import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//takes user input
		
		boolean done = false;//flag control variable
		while (!done) {
		System.out.println("1. Say Hello ");//prompting user
		System.out.println("2. Addition ");//prompting user
		System.out.println("3. Multiplication ");//prompting user
		System.out.println("4. Exit ");//prompting user
		System.out.print("Choice: ");//prompting user for input
		String choice = input.nextLine();
		
		
		switch (choice) {
			case "1":
				System.out.println("Hello");
				break; 
			case "2":
				System.out.print("Enter a number: ");//prompting user for input
				int a = Integer.parseInt(input.nextLine());
				System.out.print("Enter another number: ");//prompting user for input
				int b = Integer.parseInt(input.nextLine());
				System.out.println("The sum is: " + (a+b));//printout the sum
				break;
			case "3":
				System.out.print("Enter a number: ");//prompting user for input
				int c = Integer.parseInt(input.nextLine());
				System.out.print("Enter another number: ");//prompting user for input
				int d = Integer.parseInt(input.nextLine());
				System.out.println("The product is: " + (c*d));//printout the product
				break;
			case "4":
				done = true;
				break;
			default :
				System.out.println("This is not a valid choice");	
		}
		
		

	}
		input.close();
		}
}
