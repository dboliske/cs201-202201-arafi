package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) throws IOException {
		File file = new File("src/labs/lab3/grades.csv"); //create file object
		Scanner input = new Scanner(file);//create scanner to read the file
		
		double total = 0.0;
		int count = 0;
		
		while (input.hasNextLine()) {
			System.out.println(input.nextLine());
			String Line = input.nextLine();
			String[] array = Line.split(",");//split the values
			double grade = Double.parseDouble(array[1]);// converts grade to double
			total += grade;
			count ++; //increment count
		}
		System.out.println("The Average of the class: "+ total/count);//printout average grade
		
		input.close();

	}

}
