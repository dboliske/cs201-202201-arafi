package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//create scanner for user input
	
		int[] array = new int [0];//create array
		boolean done = false;//flag control variable
		
		while (!done) {
			System.out.print("Enter a number or 'done' to finish :");//prompt user for input
			String number = input.nextLine();
			
			if (number.equalsIgnoreCase("done") ) {
			done = true;
			}
			else {
				
				int[] newArray = new int [array.length + 1];//create another array 
				int num = Integer.parseInt(number);//convert string to integer
				
				for (int i = 0; i< array.length; i++)	{
					newArray[i] = array[i];//assigning older array to newer array
				}
				array = newArray;//assigning newArray to array
				array[array.length -1] = num;//assigning num value in the last element of array
		}
		
	}
		for (int i=0; i<array.length; i++) {
			System.out.println(array[i]);//printout the array
		}
		
		try {
			
			System.out.print("Enter a file name: ");//prompt user for input
			String name = input.nextLine();
			
			FileWriter f = new FileWriter("src/labs/lab3/"+ name);//create file 
			for (int i = 0; i<array.length; i++ ) {
				f.write(array[i] + "\n");//write array elements to file
			}
			
			f.flush();
			f.close();
			
		}catch (IOException e) {
			System.out.println(e.getMessage());	
		}			
		input.close();
}



}










