package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		GeoLocation g1 = new GeoLocation(); //create new instance to call default constructors
		
		System.out.println(g1.toString()); //printout the instance with default constructors
		
		g1.setlat(50); //checking the mutator method
		System.out.println(g1);
		
		g1.setlat(100); //checking the mutator method
		System.out.println(g1);
		
		g1.setlng(50); //checking the mutator method
		System.out.println(g1);
		
		g1.setlng(200); //checking the mutator method
		System.out.println(g1);
		
		GeoLocation g2 = new GeoLocation(20,20); //create new instance with new parameters 
		System.out.println(g2);
		System.out.println(g1.equals(g2));
		
		GeoLocation g3 = new GeoLocation(60,100); //create new instance with new parameters 
		System.out.println(g3);
		System.out.println(g1.equals(g3));
		

	}

}
