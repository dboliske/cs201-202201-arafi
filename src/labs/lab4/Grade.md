# Lab 4

## Total

27/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7.75/8
  * Application Class   0.5/1
* Part II PhoneNumber
  * Exercise 1-9        6.75/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        7/8
  * Application Class   1/1
* Documentation         3/3

## Comments
Part1:
-0.25 you do not call the setters in the default constructor
-0.5 You needed to call the accessors (getters) in the application class
Part2:
-1 Variables have to be private as shown in the png
-0.25 Messy non-default constructor (you either call the setters or you use 'this' keyword)
Part3:
-1 Variables have to be private as shown in the png