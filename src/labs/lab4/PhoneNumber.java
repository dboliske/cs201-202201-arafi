package labs.lab4;

public class PhoneNumber {

	public String countryCode; //Attribute, instance variable
	public String areaCode; //Attribute, instance variable
	public String number; //Attribute, instance variable
	
	public PhoneNumber() { //default constructors
		countryCode = "123";
		areaCode = "456";
		number = "7891234";
	}
	
	public PhoneNumber(String cCode, String aCode, String number) { //non-default constructors
		countryCode = cCode;
		setCountryCode(cCode);
		areaCode = aCode;
		setAreaCode(aCode);
		this.number = number;
		setNumber(number);
	}
	
	public String getCountryCode() { //accessor method
		return countryCode;
	}
	
	public String getAreaCode() { //accessor method
		return areaCode;
	}
	
	public String getNumber() { //accessor method
		return number;
	}
	
	public void setCountryCode(String cCode) { //mutator method
		this.countryCode = cCode;
	}
	
	public void setAreaCode(String aCode) { //mutator method
		if (aCode.length()==3) {
			this.areaCode = aCode;
		}
		
	}
	
	public void setNumber(String number) { //mutator method
		if (number.length()==7) {
			this.number = number;
		}
		
	}
	
	public String toString() { //toString method to return value
		return (countryCode + "-" + areaCode + "-" + number );
	}
	
	public boolean validAreaCode(String aCode) { //validating the data entered
		if (aCode.length()==3) {
			return true;
		}
		return false;
	}
	
	public boolean validNumber(String number) { //validating the data entered
		if (number.length()==7) {
			return true;
		}
		return false;
	}
	
	public boolean equals(PhoneNumber pn) {  //equals method to compare two instances
		if(this.countryCode != pn.getCountryCode() ) {
			return false;
		}
		if(this.areaCode != pn.getAreaCode()) {
			return false;
		}
		return true;
	}
	
	
}
