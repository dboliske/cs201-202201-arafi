package labs.lab4;

public class PhoneNumberUser {

	public static void main(String[] args) {
		PhoneNumber pn1 = new PhoneNumber(); //create new instance to call default constructors
		
		System.out.println(pn1.toString()); //printout the instance with default constructors

		pn1.setCountryCode("567"); //checking the mutator method
		System.out.println(pn1);
		
		pn1.setAreaCode("444"); //checking the mutator method
		System.out.println(pn1);
		 
		pn1.setAreaCode("4445"); //checking the mutator method
		System.out.println(pn1);
		
		pn1.setNumber("0987654"); //checking the mutator method
		System.out.println(pn1);
		
		pn1.setNumber("09876549999"); //checking the mutator method
		System.out.println(pn1);
		
		
		PhoneNumber pn2 = new PhoneNumber("222","888","9898989"); //create new instance with new parameters
		System.out.println(pn2);
		System.out.println(pn1.equals(pn2));
		
		PhoneNumber pn3 = new PhoneNumber("567","444","0987654"); //create new instance with new parameters
		System.out.println(pn3);
		System.out.println(pn1.equals(pn3));
		
	}

}
