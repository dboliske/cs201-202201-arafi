package labs.lab4;

public class Potion {

	public String name; //Attribute, instance variable
	public double strength; //Attribute, instance variable
	
	public Potion() { //default constructors
		name = "ethanol";
		strength = 5.0;
	}
	
	public Potion(String name, double strength) { //non-default constructors
		this.name = name;
		setName(name);
		this.strength = strength;
		setStrength(strength);
	}
	
	public String getName() { //accessor method
		return name;
	}
	
	public double getStrength() { //accessor method
		return strength;
	}
	
	public void setName(String name) { //mutator method
			this.name = name;
	}
	
	public void setStrength(double strength) { //mutator method
		if(strength >= 0.0 && strength <= 10.0) {
			this.strength = strength;
		}
	}
	
	public String toString() { //toString method to return value in specified format
		return (name + " has strength of " + strength);
	}
	
	public boolean validStrength(double Strength) { //validating the data entered
		if(this.strength >= 0 && this.strength <= 10) {
			return true;
		}
		return false;
	}
	
	public boolean equals(Potion p) { //equals method to compare two instances
		if (this.name != p.getName()) {
			return false;
		} else if (this.strength != p.getStrength()) {
			return false;
		}
		return true;
	}
}
