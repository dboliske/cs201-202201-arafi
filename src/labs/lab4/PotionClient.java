package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		Potion p1 = new Potion(); //create new instance to call default constructors
		
		System.out.println(p1.toString()); //printout the instance with default constructors
		
		p1.setName("propane"); //checking the mutator method
		System.out.println(p1);
		
		p1.setStrength(8.0); //checking the mutator method
		System.out.println(p1);
		
		p1.setStrength(20.0); //checking the mutator method
		System.out.println(p1);
		
		Potion p2 = new Potion("alkene", 7.0); //create new instance with new parameters
		System.out.println(p2);
		System.out.println(p1.equals(p2));
		
		Potion p3 = new Potion("propane", 8.0); //create new instance with new parameters
		System.out.println(p3);
		System.out.println(p1.equals(p3));
		

	}

}
