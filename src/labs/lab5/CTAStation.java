package labs.lab5;

import java.util.Objects;

public class CTAStation extends GeoLocation {

	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		super();
		name = "harlem";
		location = "chicago";
		wheelchair = false;
		open = false;
	}
	
	public CTAStation(String name, double lat, double lng,  String location, boolean wheelchair, boolean open) {
		super(lat,lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		return "The " + name + " Station is located in " + super.toString() + "," + location;
	}
	
//	@Override
//	public boolean equal(Object obj) {
//		if (!super.equals(obj)) {
//			return false;
//		} else if (!(obj instanceof GeoLocation)) {
//			return false;
//		}
//		
//		CTAStation c = (CTAStation)obj;
//		if (this.name != (c.getName())) {
//			return false;
//		} else if (this.location != c.getLocation()) {
//			return false;
//		} else if (this.wheelchair != c.hasWheelchair()) {
//			return false;
//		} else if (this.open != c.isOpen()) {
//			return false;
//		}
//		
//		return true;
//	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CTAStation other = (CTAStation) obj;
		return Objects.equals(location, other.location) && Objects.equals(name, other.name) && open == other.open
				&& wheelchair == other.wheelchair;
	}
	
	
	
	
	
}
