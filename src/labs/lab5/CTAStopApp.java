package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTAStopApp {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		CTAStation[] data = readFile("CTAStops");
		menu(data);
		input.close();
		System.out.println("GoodBye!!");
	}

	public static CTAStation[] readFile(String filename) {
		CTAStation[] ctaStation = new CTAStation[34];
		int count = 0;
		
		try {
			File c = new File("src/labs/lab5/"+filename+".csv");
			Scanner input = new Scanner(c);
			String line = input.nextLine();
			while (input.hasNextLine()) {
				
				String[] values = input.nextLine().split(",");
				
				
				 CTAStation a = new CTAStation(values[0],
						Double.parseDouble(values[1]), 
						Double.parseDouble(values[2]), 
						values[3], 
						Boolean.parseBoolean(values[4]), 
						Boolean.parseBoolean(values[5]));
							
				if (ctaStation.length == count) {
					ctaStation = resize(ctaStation, ctaStation.length*2);
				}
				
				ctaStation[count] = a ;
				count++;
			}
			input.close();
			
			
		} catch (Exception e) {
		
		}
		ctaStation = resize(ctaStation,count);
		return ctaStation;
	}
	
	public static CTAStation[] resize(CTAStation[] station, int size) {
		CTAStation[] temp = new CTAStation[size];
		int limit = station.length > size ? size : station.length;
		for (int i=0; i<limit; i++) {
			temp[i] = station[i];
		}
		
		return temp;
	}
	
	public static void menu(CTAStation[] station) {
		boolean done = false;
		Scanner input = new Scanner(System.in);
		do {
			System.out.println("1. Station Names ");
			System.out.println("2. Stations with/without wheelchair ");
			System.out.println("3. Nearest Station ");
			System.out.println("4. Exit ");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			
			switch (choice) {
				case "1":
					displayStationNames(station);
					break;
				case "2":
					displayByWheelchair(input,station);
					break;
				case "3":
					displayNearest(input,station);
					break;
				case "4":
					done = true;
					break;
				default:
					System.out.println("Invalid option");	
			}
			
		}while(!done);
		input.close();
	
	}
	
	public static void displayStationNames(CTAStation[] station) {
		for (int i =0; i<station.length; i++) {
			System.out.println(station[i].getName());
		}
	}
	
	public static void displayByWheelchair(Scanner input, CTAStation[] station ) {
		boolean result = false;
		while (!result) {
			System.out.print("Do you want wheelchair? ('y' or 'n') ");
			String yn = input.nextLine();
			switch (yn.toLowerCase()) {
				case "y":
					int count = 0;
					for (int i=0; i<station.length;i++) {
						if (station[i].hasWheelchair() == true) {
							System.out.println(station[i].getName() + " has wheelchair");
							count++;
						} 
					}
					if (count == 0 ) {
						System.out.println("No Station found");
					}
					result = true;
					break;
					
				case "n":
					int count1 = 0;
					for (int j =0; j<station.length; j++) {
						if (station[j].hasWheelchair() == false) {
							System.out.println(station[j].getName() + " does not have wheelchair");
							count1++;
						}
					}
					if (count1 == 0 ) {
						System.out.println("No Station found");
					}
					result = true;
					break;
				default:
					System.out.println("Invalid! ");
			}
		}
	}
	
	
	public static void displayNearest(Scanner input, CTAStation[] station) {
		
		System.out.print("Enter a latitude: ");
		double lat = Double.parseDouble(input.nextLine());
		System.out.print("Enter a longitude: ");
		double lng = Double.parseDouble(input.nextLine());
		
		double nearest = station[0].calcDistance(lat, lng);
		
		String name = null;
		for (int i=0; i<station.length; i++){
			if (station[i].calcDistance(lat,lng) < nearest) {
				nearest = station[i].calcDistance(lat,lng);
			    name = station[i].getName();
			}
			
		}
		System.out.println("the nearest is "+ name);
	}
}






















