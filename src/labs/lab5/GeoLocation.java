package labs.lab5;

public class GeoLocation {

	private double lat; //Attribute, instance variable
	private double lng; //Attribute, instance variable
	
	
	public GeoLocation() {   //default constructors 
		lat = 0.0;
		lng = 0.0;
		
	}
	
	public GeoLocation(double lat, double lng) { //non-default constructors
		setlat(lat);
		setlng(lng);
	}
	
	public double getlat() { //accessor method
		return lat;
	}
	
	public double getlng() { //accessor method
		return lng;
	}
	
	public void setlat(double lat) { //mutator method
		if (lat >=-90 && lat <= 90) {
			this.lat = lat;
		}
	}
	
	public void setlng(double lng) { //mutator method
		if (lng >=-180 && lng <= 180 ) {
			this.lng = lng;
		}
	}
		
	public String toString() { //toString method to return value in the format (lat,lng)
		return ( "(" + lat + "," + lng + ")" );
	}
	
	public boolean validlat(double lat) { //validating the data entered is within the range
		if (lat >= -90 && lat <= 90  ) {
			return true;
		}
		return false;
	}
	
	public boolean validlng(double lng) { //validating the data entered is within the range
		if (lng >=-180 && lng <= 180 ) {
			return true;
		}
		return false;
	}
	
	public boolean equals(GeoLocation g) { //equals method to compare two instances
		if (this.lat != g.getlat()) {
			return false;
		}
		if (this.lng != g.getlng()) {
			return false;
		}
		return true;
	}
	
	
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(this.lat - g.getlat(), 2) + Math.pow(this.lng - g.getlng(), 2));
	}
	
	public double calcDistance(double lat2, double lng2) {
		return Math.sqrt(Math.pow(this.lat - lat2, 2) + Math.pow(this.lng - lng2, 2));

		
		

	}
}
