package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounter {
	
	public static ArrayList<String> addCustomer(Scanner input, ArrayList<String> customer){
	
		boolean done = false;
		
		do {	
			System.out.print("Enter a name or 'done' to exit: ");
			String name1 = input.nextLine();
			if (name1.equals("done")) {
				done = true;
			}
			else {
				customer.add(name1);
				System.out.println("Position in queue: " + customer.size());
			}
		}while (!done);
		
		//for (String c: customer) {
			//System.out.println(c);
		//}
	
		
	return customer;
	}
	
	
	public static void menu(Scanner input, ArrayList<String> customer) {
		boolean done = false;
		
		do {
			System.out.println("1. Add customer to queue: ");
			System.out.println("2. Help customer");
			System.out.println("3. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1":
					addCustomer(input,customer);
					break;
				case "2":
					helpCustomer(customer);
					break;
				case "3":
					done = true;
					break;
				default:
					System.out.println("Invalid option");
				
			}
			
		}while(!done);
	}
	
	
	public static ArrayList<String> helpCustomer(ArrayList<String> customer) {
		
		if (customer.size() == 0) {
			System.out.println("No one in the queue");
		}
		else {
			String a = customer.get(0);
			customer.remove(0);
			System.out.println("Name: " + a);
		}
			return customer;
	}
	

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		ArrayList<String> customer	= new ArrayList<String>();
		menu(input,customer);
		
		input.close();
		System.out.println("GoodBye!!");

	}

}
