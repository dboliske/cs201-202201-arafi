package labs.lab7;

public class BubbleSort {
	
	public static int[] sort(int[] newArray) {
		boolean done = false;
		
		do {
			done = true;
			for (int i = 1; i<newArray.length; i++) {
				if(newArray[i-1] > newArray[i]) {
					int a = newArray[i-1];
					newArray[i-1] = newArray[i];
					newArray[i] = a;
					done = false;
				}
			}
		}while(!done);
		
		return newArray;
	}
	
	
	public static void main(String[] args) {
		int[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		array = sort(array);
		
		for (int i : array) {
			System.out.print(i + " ");
		}

	}

}
