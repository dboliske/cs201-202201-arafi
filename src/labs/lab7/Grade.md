# Lab 7

## Total

17/20

## Break Down

- Exercise 1 4/4
- Exercise 2 5/5
- Exercise 3 5/5
- Exercise 4 3/6

## Comments
Good work. However, you do not implement Binary Search as recursive method in the last question.