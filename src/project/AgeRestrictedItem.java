package project;					//This class is a subclass of the general store application, and it inherits from ShelvedItem class. 
									//It has an attributes 'age'.
									//Name: Abrar Shahariar Rafi; Date: 04/29/2022

public class AgeRestrictedItem extends ShelvedItem {

	private int age;
	
	public AgeRestrictedItem() {
		super();
		age = 0;
	}
	
	public AgeRestrictedItem(String name,double price, int age) {
		super(name,price);
		setAge(age);
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		if(age > 0) {
			this.age = age;
		}
	}
	
	@Override
	public String toString() {
		return super.toString() + "," + age;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) {
			return false;
		}
		
		AgeRestrictedItem a =(AgeRestrictedItem) obj;
		if ( age != a.getAge()) {
			return false;
		}
		
		return true;
	}
	protected String csvData() {
		return super.csvData() + "," + age;
	}
	
	
	

}
