package project; 					//This is the application class of the general store application.
									//It contains a lot of methods for specific task, such as readfile, menu, add item, deduct item, modify and search item methods.
									//Name: Abrar Shahariar Rafi; Date: 04/29/2022

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Application {
	
	public static ArrayList<ShelvedItem> readData(String filename){			//This method is used to read data from a file, and store them in an ArrayList. The filename is provided by the user.
		ArrayList<ShelvedItem> stock = new ArrayList<ShelvedItem>();		
		 
		try {
			File f = new File(filename);
			Scanner in = new Scanner(f);
			while (in.hasNextLine()) {
				
				try {
					
					String[] item = in.nextLine().split(",");           //item is a string array
					
					String name = item[0];                              //String variable 'name' contains the first element of item array
					double price = Double.parseDouble(item[1]);
					
					if (item.length == 2) {
						ShelvedItem S = new ShelvedItem(name,price);
						stock.add(S);
					}
					
					else {

						if (item[2].length()>=6 && item[2].length()<=10) {
							 ProduceItem P = new ProduceItem(name,price,item[2]);
							 stock.add(P);
						} 
						
						  
						if (item[2].length()>0 && item[2].length()<3 ) {
							AgeRestrictedItem A = new AgeRestrictedItem(name,price,Integer.parseInt(item[2]));
							stock.add(A);
						}
						
					}	
					
					
				} catch (Exception e) {
					System.out.println("Error");
				}
	
			}
				
			in.close();
		} catch (Exception e) {
			System.out.println("Error reading the data");
		}
		
		return stock;
	}

	
	
	public static ArrayList<ShelvedItem> menu(ArrayList<ShelvedItem> stock, ArrayList<ShelvedItem> item){    //This method displays the menu to user. Depending on user choice, it calls other methods.
		Scanner input = new Scanner(System.in);
		
		boolean done = false;
		
		do {
			System.out.println("1. Add item");
			System.out.println("2. Deduct item");
			System.out.println("3. Search item");
			System.out.println("4. Modify item");
			System.out.println("5. Save item");
			System.out.println("6. Exit");
			
			System.out.print("Choice: ");
			String in = input.nextLine();
			
			switch (in) {
				case "1":
					stock = addItem(stock,input);     //calls addItem method
					break;
				case "2":
					stock = deductItem(stock,item,input);  //calls deductItem method
					break;
				case "3":
					searchItem(stock,input);      //calls searchItem method
					break;
				case "4":
					stock = modifyItem(stock,input);   //calls modifyItem method
					break;
				case "5":
					System.out.print("Name of file: ");                     //promts user for filename to save data
					String a = input.nextLine();							//calls saveFile method
					String filename = "src/project/" + a + ".csv";
					saveFile(filename,stock);
					break;
				case "6":
					done = true;
					break;
				default:
						System.out.println("Please try again");
			}
		}while(!done);
		
		System.out.println("Goodbye!");
		
		return stock;
	}
	
	public static ArrayList<ShelvedItem> addItem(ArrayList<ShelvedItem> stock, Scanner input){  //This method allows user to add item to the stock, that is, adding to the ArrayList. It prompts user what type of item they want to add, and ask further information depending on user input.
		
		System.out.println("What Item? ");
		System.out.println("1. Shelved item ");
		System.out.println("2. Produce item ");
		System.out.println("3. Age Restricted item ");
		System.out.print("Choice: ");
		int in = Integer.parseInt(input.nextLine());
		System.out.println("Name:");
		String name = input.nextLine();
		System.out.println("Price: ");
		Double price = Double.parseDouble(input.nextLine());
		
		switch (in) {
			case 1:
				System.out.print("no. of items: ");						//prompts user for the number of items they want to add
				int n = Integer.parseInt(input.nextLine());
				for(int i=0; i<n; i++) {   								//adds item to the ArrayList, the number of times specified by the user
					ShelvedItem S = new ShelvedItem(name,price);
					stock.add(S);										//adds shelved item to the arrayList
					S = null;
				}
				break;
			case 2:
				System.out.print("Expiry date (mm/dd/yyyy): ");			//prompts user for the expiry date
				String date = input.nextLine();
				System.out.print("no. of items: ");
				int m = Integer.parseInt(input.nextLine());
				for(int i=0; i<m; i++) {
					ProduceItem P = new ProduceItem(name,price,date);
					stock.add(P);										//adds produce item to the arrayList
					P = null;
				}			
				break;
			case 3:
				System.out.print("Age: ");								//prompts user for the age
				int age = Integer.parseInt(input.nextLine());
				System.out.print("no. of items: ");
				int l = Integer.parseInt(input.nextLine());
				for(int i=0; i<l; i++) {
					AgeRestrictedItem A = new AgeRestrictedItem(name,price,age);
					stock.add(A);													//adds age restricted item to the arrayList
					A = null;
				}			
				break;
			default:
					System.out.println("Not an option");
		}
		
		return stock;
	}
	
	public static ArrayList<ShelvedItem> deductItem(ArrayList<ShelvedItem> stock, ArrayList<ShelvedItem> item, Scanner input){  //This method removes item from the stock. It calls the search method to search for the item specified by the user and removes it.
		int m = searchItem(stock,input);
		if (m == -1) {
			System.out.println("Cannot Remove");
		} else {
			item.add(stock.get(m));
			stock.remove(m);
			System.out.println(stock.get(m).getName() + " removed");
		}	
		
		return stock;
	}
	
	public static int searchItem(ArrayList<ShelvedItem> stock, Scanner input){   //This method is used to search item by the name of the item provided by the user.
		System.out.println("Name: ");
		String name = input.nextLine();
		int a =-1;
			for(int i =0; i<stock.size();i++) {
				if (name.equalsIgnoreCase(stock.get(i).getName())) {
					a = i;
					System.out.println(stock.get(i).getName() +" found");
					return a;
				}
			}
		System.out.println("Item not found");
		return a;
	}
	
	public static ArrayList<ShelvedItem> modifyItem(ArrayList<ShelvedItem> stock, Scanner input){   //This method is used to modify the name or price of an item specified by the user. It calls the search method to search for the item and modifies the name or price as specified by the user.
		int m = searchItem(stock,input);
		if (m == -1) {
			System.out.println("Cannot modify");
			return stock;
		}
		
		boolean done = false;
		do {
			try {
				System.out.println("Which to edit?");						
				System.out.println("1. Name");
				System.out.println("2. price");
				System.out.println("3. Exit");
				System.out.print("Choice: ");									//prompts user for choice from the menu
				int in = Integer.parseInt(input.nextLine());
				switch(in) {
					case 1:
						System.out.println("Current Name: " + stock.get(m).getName());
						System.out.print("Name: ");
						stock.get(m).setName(input.nextLine());					//Modifies the name of the item
						break;
					case 2:
						System.out.println("Current price: " + stock.get(m).getPrice());
						System.out.print("Price: ");
						stock.get(m).setPrice(Double.parseDouble(input.nextLine()));  //modifies the price of the item
						break;	
					case 3:
						done = true;
						break;
					default:
						System.out.println("Not an option");
			}
				
			}catch (Exception e) {
				System.out.println("Not a valid option");
			}
			
			
		}while(!done);
		
		
		return stock;
	}
	
	public static void saveFile(String filename, ArrayList<ShelvedItem> stock) {   //This method writes data into a file. It asks the user for a filename and saves all the data into that file. 
		try {
			FileWriter writer = new FileWriter(filename);				//writes the data into the file 
			
			for (int i=0; i<stock.size(); i++) {
				writer.write(stock.get(i).csvData() + "\n");
				writer.flush();
			}
			
			writer.close();
		} catch (Exception e) {
			System.out.println("Error saving to file.");
		}
	}
	
	
	
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("Filename: ");
		String filename = in.nextLine();  //filename stores the name of the file provided by the user. in this case it should be 'stock'
		
		ArrayList<ShelvedItem> stock = readData("src/project/"+ filename + ".csv");  //stock is the ArrayList that stores data from the stock file
		
		ArrayList<ShelvedItem> item = new ArrayList<ShelvedItem>();  //Item is an ArrayList of initial capacity of 10
		
		stock = menu(stock,item);       //Calls the menu method. ArrayList stock and item is fetched to the menu method
		
		
		in.close();
	}

}




			