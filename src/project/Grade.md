# Final Project

## Total

138/150

## Break Down

Phase 1:                                                            50/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      4/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            5/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           7/7
- UML Class Diagrams                                                10/10
- Testing Plan                                                      10/10

Phase 2:                                                            88/100

- Compiles and runs with no run-time errors                         8/10
- Documentation                                                     15/15
- Test plan                                                         5/10
- Inheritance relationship                                          5/5
- Association relationship                                          5/5
- Searching works                                                   3/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           12/15
- Project generates paths between any two stations                  -/10#RUBRIC IGNORED
- Project encapsulates data                                         5/5
- Project is well coded with good design                            10/10
- All classes are complete (getters, setters, toString, equals...)  5/5

## Comments
Your project is good, but to be honest I found it a little boring.
Just a little advice, make your projects fun! you can do that by adding fun print statements or just impoving the user interface because it is the interaction tool between the user and the program.
Good work overall!
### Design Comments
Design looks good
### Code Comments
-For the Add item option in the menu, the code gives a run-time error when I do not enter a valid choice for the item's category.
-Test plan was not modified since project design. The new test plan you included was not a test plan
-The goal behind the searching method is not only to tell the user whether the item was found. Rather, it should give the information of the item when name is entered.
-In the deduct method, your program should allow the user to add the item to a cart first before choosing to check out.
