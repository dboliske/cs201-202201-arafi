package project;                		//This class is a subclass of the general store application, and it inherits from ShelvedItem class. 
 										//It has an attributes 'expiry date'.
										//Name: Abrar Shahariar Rafi; Date: 04/29/2022

public class ProduceItem extends ShelvedItem {

	private String eDate;
	
	public ProduceItem() {
		super();
		eDate = "12/12/2020" ;
	}
	
	public ProduceItem(String name, double price, String eDate) {
		super(name,price);
		this.eDate = eDate;
	}

	public String geteDate() {
		return eDate;
	}

	public void seteDate(String eDate) {
		this.eDate = eDate;
	}
	
	@Override
	public String toString() {
		return  super.toString() + "," + eDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		 if (obj == null) {
				return false;
			} else if (this == obj) {
				return true;
			} else if (!(obj instanceof ProduceItem)) {
				return false;
			}
		 
		 ProduceItem p = (ProduceItem) obj;
		 if(!this.eDate.equals(p.geteDate())) {
			 return false;
		 }
		 
		 return true;
	} 
	protected String csvData() {
		return super.csvData() + "," + eDate;
	}
	
	
}

