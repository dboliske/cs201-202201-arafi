package project;                     //This class is the superclass of the general store application. It has two attributes 'name' and 'price'.
									 //Name: Abrar Shahariar Rafi; Date: 04/29/2022

public class ShelvedItem {

	private String name;
	private double price;
	
	public ShelvedItem() {
		name = "banana";
		price = 0.0;
	}
	
	public ShelvedItem(String name, double price) {
		
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if(price >= 0.0) {
			this.price = price;
		}
	}
	
	@Override
	public String toString() {
		return name + "," + price;
	}
	
	@Override
	public boolean equals(Object obj) {
		 if (obj == null) {
				return false;
			} else if (this == obj) {
				return true;
			} else if (!(obj instanceof ShelvedItem)) {
				return false;
			}
		 
		 ShelvedItem s = (ShelvedItem) obj;
		 if (!name.equals(s.getName())) {
			 return false;
		 } else if ( price != s.getPrice()) {
			 return false;
		 }
		 
		 return true;
	 }
	
	protected String csvData() {
		return name + "," + price;
	}
	

	
}
	

